#include <iostream>
#include <stdexcept>
#include <initializer_list>
#include <string>

using namespace std;

template<typename T>
struct to_string_convert_t
{
public:
	static string convert(T value)
	{
		return to_string(value);
	}
};

template<typename T>
struct to_string_convert_t<T*>
{
public:
	static string convert(T* value)
	{
		return to_string_convert_t<T>::to_string(*value);
	}
};

template<typename T>
class node_t
{
private:
	T value = nullptr;
	node_t<T>* __xor = nullptr;

public:
	T get_value() const noexcept
	{
		return value;
	}

	void set_value(T value)
	{
		this->value = value;
	}

	node_t<T>* get_xor() const noexcept
	{
		return __xor;
	}

	void set_xor(node_t<T>* value)
	{
		__xor = value;
	}

	node_t(T value, node_t<T>* nextPrevXor) : value(value), __xor(nextPrevXor)
	{
	}

	node_t(const node_t<T>& other) : value(other.GetValue()), __xor(other.GetNextPrevXor())
	{
	}

	string to_string()
	{
		return to_string_convert_t<T>::convert(value);
	}

	friend ostream& operator<<(ostream& output, const node_t<T>& node)
	{
		output << node.to_string();
		return output;
	}
};

template<typename T>
node_t<T>* make_xor(node_t<T>* first, node_t<T>* second)
{
	return (node_t<T>*)((uintptr_t)first ^ (uintptr_t)second);
}

template<typename T>
class xor_linked_list_t
{
private:
	uint32_t count = 0;
	node_t<T>* head = nullptr;
	node_t<T>* tail = nullptr;

	const string ContainerWasEmptyMsg = "Container was empty";

public:
	uint32_t get_count() const noexcept
	{
		return count;
	}

	node_t<T>* get_head() const noexcept
	{
		return head;
	}

	node_t<T>* get_tail() const noexcept
	{
		return tail;
	}

	xor_linked_list_t(const initializer_list<T>& source)
	{
		auto it = source.begin();
		while (it != source.end())
			addLast(*it++);
	}

	void addFirst(T item)
	{
		node_t<T>* newNode = new node_t<T>(item, make_xor<T>(nullptr, head));
		if (count == 0)
			tail = newNode;
		else
		{
			node_t<T>* next = make_xor<T>(nullptr, head->get_xor());
			head->set_xor(make_xor<T>(newNode, next));
		}
		head = newNode;
		count++;
	}

	void addLast(T item)
	{
		node_t<T>* newNode = new node_t<T>(item, make_xor<T>(tail, nullptr));
		if (count == 0)
			head = newNode;
		else
		{
			node_t<T>* previous = make_xor<T>(tail->get_xor(), nullptr);
			tail->set_xor(make_xor<T>(previous, newNode));
		}
		tail = newNode;
		count++;
	}

	void removeFirst()
	{
		if (count == 0)
			throw logic_error(ContainerWasEmptyMsg);
		if (count == 1)
			head = tail = nullptr;
		else
		{
			node_t<T>* second = make_xor<T>(nullptr, head->get_xor());
			node_t<T>* nextToSecond = make_xor<T>(head, second->get_xor());
			second->set_xor(make_xor<T>(nullptr, nextToSecond));
			delete tail;
			head = second;
		}
		count--;
	}

	void removeLast()
	{
		if (count == 0)
			throw logic_error(ContainerWasEmptyMsg);
		if (count == 1)
			head = tail = nullptr;
		else
		{
			node_t<T>* preTail = make_xor<T>(tail->get_xor(), nullptr);
			node_t<T>* previousToPreTail = make_xor<T>(preTail->get_xor(), tail);
			preTail->set_xor(make_xor<T>(previousToPreTail, nullptr));
			delete tail;
			tail = preTail;
		}
		count--;
	}

	void clear()
	{
		head = tail = nullptr;
		count = 0;
	}

	friend ostream& operator<<(ostream& output, const xor_linked_list_t<T>& list)
	{
		node_t<T>* previous = nullptr;
		node_t<T>* current = list.get_head();
		uint32_t index = 0;

		while (index < list.get_count())
		{
			output << current->to_string();
			if (index + 1 < list.get_count())
				output << ", ";
			node_t<T>* next = make_xor(previous, current->get_xor());
			previous = current;
			current = next;
			index++;
		}

		return output;
	}
};

int main()
{
	xor_linked_list_t<int> x{ 1, 2, 3 };
	x.addLast(4);
	x.removeLast();
	cout << x;
	return 0;
}